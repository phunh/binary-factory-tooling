// Request a node to be allocated to us
node( "WindowsMingw" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {

		// First Thing: Checkout Sources
		stage('Checkout Sources') {
			// Make sure we have a clean slate to begin with
			deleteDir()

			// Krita Code
			checkout changelog: true, poll: true, scm: [
				$class: 'GitSCM',
				branches: [[name: 'krita/5.1']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'krita/']],
				userRemoteConfigs: [[url: 'https://invent.kde.org/graphics/krita.git']]
			]

		}

		// Now retrieve the artifacts
		stage('Retrieving Dependencies') {
			// First we grab the artifacted dependencies built last time round
			copyArtifacts filter: 'krita-deps.zip', projectName: 'Krita_Nightly_Windows_Dependency_Build'

			// Now we unpack them
			bat """
				mkdir C:\\Packaging\\KritaWS\\
				cd C:\\Packaging\\KritaWS\\
				C:\\Craft\\Krita\\windows-msvc2019_64-cl\\dev-utils\\bin\\7za.exe x "%WORKSPACE%\\krita-deps.zip"
			"""
		}

		// Now we can build Krita
		stage('Building Krita') {
			// Before we can do this we need to setup some environment variables to help guide the Krita scripts around
			// In particular it needs:
			// PATH set to guide it to CMake and to allow it to find Mingw (Git and Python are already in PATH as standard convention for these builders)
			// SEVENZIP_EXE set to guide it to 7-Zip's console utility. It's detection support doesn't work as our binary is named 7za.exe instead of the apparently standard 7z.exe
			// Then we can run it
			// We also set SIGNTOOL_SIGN_FLAGS which is used by the package signing script when invoking signtool.exe
			bat """
				set PATH=C:/MingW-LLVM/bin;C:/Program Files/Python310/;C:/Craft/Krita/windows-msvc2019_64-cl/bin/;C:/Craft/Krita/windows-msvc2019_64-cl/dev-utils/bin/;C:/Craft/Krita/windows-msvc2019_64-cl/dev-utils/svn/bin/;%PATH%
				set SEVENZIP_EXE=C:/Craft/Krita/windows-msvc2019_64-cl/dev-utils/bin/7za.exe
				set GETTEXT_SEARCH_PATH=C:\\Craft\\Krita\\windows-msvc2019_64-cl\\bin

				cmd /c krita\\build-tools\\windows\\build.cmd --no-interactive --skip-deps --src-dir "%WORKSPACE%\\krita" --download-dir "%WORKSPACE%\\downloads" --krita-build-dir "%WORKSPACE%\\build" --deps-install-dir "C:\\Packaging\\KritaWS\\deps-install" --krita-install-dir "C:\\Packaging\\KritaWS\\krita-install" 
				
				if errorlevel 1 exit /b %errorlevel%

				pushd krita
				for /f usebackq %%i in (`git rev-parse --short HEAD`) do set GIT_REVISION=%%i
				for /f "tokens=3 usebackq" %%a in (`findstr /C:"#define KRITA_VERSION_STRING" %WORKSPACE%\\build\\libs\\version\\kritaversion.h`) do (
					set "PACKAGE_SUFFIX=%%a"
				)
				SET PACKAGE_SUFFIX=###%PACKAGE_SUFFIX%###
				SET PACKAGE_SUFFIX=%PACKAGE_SUFFIX:"###=%
				SET PACKAGE_SUFFIX=%PACKAGE_SUFFIX:###"=%
				SET PACKAGE_SUFFIX=%PACKAGE_SUFFIX:###=%
				SET PACKAGE_SUFFIX=%PACKAGE_SUFFIX%-%GIT_REVISION%
				popd

				set SIGNTOOL_SIGN_FLAGS=/v /n "K Desktop Environment e.V." /tr "http://timestamp.digicert.com" /td SHA256 /fd SHA256

				cmd /c krita\\packaging\\windows\\package-complete.cmd --no-interactive --src-dir "%WORKSPACE%\\krita" --deps-install-dir "C:\\Packaging\\KritaWS\\deps-install" --krita-install-dir "C:\\Packaging\\KritaWS\\krita-install"  --pre-zip-hook "%WORKSPACE%\\krita\\packaging\\windows\\sign-package.cmd" --package-name "krita-nightly-x64-%PACKAGE_SUFFIX%"
				
				if errorlevel 1 exit /b %errorlevel%
			"""
		}

		stage('Generating Installer') {
			// Let's build an installer!
			bat """
				pushd krita
				for /f usebackq %%i in (`git rev-parse --short HEAD`) do set GIT_REVISION=%%i
				for /f "tokens=3 usebackq" %%a in (`findstr /C:"#define KRITA_VERSION_STRING" %WORKSPACE%\\build\\libs\\version\\kritaversion.h`) do (
					set "PACKAGE_SUFFIX=%%a"
				)
				SET PACKAGE_SUFFIX=###%PACKAGE_SUFFIX%###
				SET PACKAGE_SUFFIX=%PACKAGE_SUFFIX:"###=%
				SET PACKAGE_SUFFIX=%PACKAGE_SUFFIX:###"=%
				SET PACKAGE_SUFFIX=%PACKAGE_SUFFIX:###=%
				SET PACKAGE_SUFFIX=%PACKAGE_SUFFIX%-%GIT_REVISION%
				popd

				set PATH=C:/MingW-LLVM/bin;C:/Program Files/Python310/;C:/Craft/Krita/windows-msvc2019_64-cl/bin/;C:/Craft/Krita/windows-msvc2019_64-cl/dev-utils/bin/;%PATH%
				set PACKAGE_NAME=krita-nightly-x64-%PACKAGE_SUFFIX%

				cd C:\\Packaging\\KritaWS
				mkdir installer
				cd installer
				cmake -DREMOVE_DEBUG=ON -DOUTPUT_FILEPATH="%WORKSPACE%\\%PACKAGE_NAME%-setup.exe" -DKRITA_PACKAGE_ROOT="%WORKSPACE%\\%PACKAGE_NAME%" -P "C:\\Packaging\\KritaWS\\krita-install\\MakeinstallerNsis.cmake"
				if errorlevel 1 exit /b 1

				call "C:/Program Files (x86)/Microsoft Visual Studio/2019/Professional/VC/Auxiliary/Build/vcvars64.bat"
				set SIGNTOOL_SIGN_FLAGS=/v /n "K Desktop Environment e.V." /tr "http://timestamp.digicert.com" /td SHA256 /fd SHA256

				signtool sign %SIGNTOOL_SIGN_FLAGS% "%WORKSPACE%\\%PACKAGE_NAME%-setup.exe"
			"""
		}

		stage('Generating MSIX Package') {
			// Make an unsigned MSIX package that can be used for Microsoft Store submission
			bat """
				pushd krita
				for /f usebackq %%i in (`git rev-parse --short HEAD`) do set GIT_REVISION=%%i
				for /f "tokens=3 usebackq" %%a in (`findstr /C:"#define KRITA_VERSION_STRING" %WORKSPACE%\\build\\libs\\version\\kritaversion.h`) do (
					set "PACKAGE_SUFFIX=%%a"
				)
				SET PACKAGE_SUFFIX=###%PACKAGE_SUFFIX%###
				SET PACKAGE_SUFFIX=%PACKAGE_SUFFIX:"###=%
				SET PACKAGE_SUFFIX=%PACKAGE_SUFFIX:###"=%
				SET PACKAGE_SUFFIX=%PACKAGE_SUFFIX:###=%
				SET PACKAGE_SUFFIX=%PACKAGE_SUFFIX%-%GIT_REVISION%
				popd

				set PATH=C:/MingW-LLVM/bin;C:/Program Files/Python310/;C:/Craft/Krita/windows-msvc2019_64-cl/bin/;C:/Craft/Krita/windows-msvc2019_64-cl/dev-utils/bin/;%PATH%
				set SEVENZIP_EXE=C:/Craft/Krita/windows-msvc2019_64-cl/dev-utils/bin/7za.exe
				set PACKAGE_NAME=krita-nightly-x64-%PACKAGE_SUFFIX%

				cd C:\\Packaging\\KritaWS
				mkdir msix
				cd msix
				set "KRITA_DIR=%WORKSPACE%\\%PACKAGE_NAME%"
				set "KRITA_SHELLEX=C:\\Packaging\\KritaWS\\installer\\krita-nsis"
				set "OUTPUT_DIR=%CD%\\out"
				cmd /c "C:\\Packaging\\KritaWS\\krita-install\\krita-msix\\build_msix.cmd"
				if errorlevel 1 exit /b 0
				rem For nightly/stable builds, don't report MSIX failure so the job still generates zip/NSIS artifacts.

				move out\\krita.msix "%WORKSPACE%\\%PACKAGE_NAME%-unsigned.msix"
			"""
		}

		// Now we capture them for use 
		stage('Capturing Build') {
			// To do this we just ask Jenkins to capture the zip file as an artifact
			archiveArtifacts artifacts: 'krita-nightly-x64-*.zip, krita-nightly-*-setup.exe, krita-nightly-*.msix', onlyIfSuccessful: true
		}
	}

	// As the Windows Slaves are permanent ones, we erase the Workspace as the last thing we do
	deleteDir()

	// Same goes for the stable workspace that we use
	bat """
	   rmdir /s /q C:\\Packaging\\KritaWS\\
	"""
}
}
